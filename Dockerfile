FROM almalinux/almalinux:8

#==========
# Args
#==========

RUN yum update -y && \
    yum install -y util-linux-ng && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    pwconv && \
    grpconv && \
    chmod 0744 /etc/shadow &&\
    chmod 0744 /etc/gshadow
